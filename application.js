require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const connexion = require('./connectionBdd');
const MangasRoute = require('./src/routes/Manga_Routes');
const userRoute = require('./src/routes/User_Routes');
const TypeMangaRoute = require('./src/routes/Types_manga_Routes');

connexion;

const app = express();

app.use(cors({
    origin: '*'
}));


app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.status(200).json({ message: 'Bienvenue sur l\'API de manga' })
})

app.use('/manga', MangasRoute);
app.use('/user', userRoute);
app.use('/typeManga', TypeMangaRoute)
module.exports = app;