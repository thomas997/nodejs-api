const mongoose = require('mongoose')
require('dotenv').config()

const connexion = mongoose.connect(process.env.URL_MONGOOSE, {

}).then(()=> {
    console.log("Connexion à la BDD effectuée")
}).catch(err => {
    console.log("une erreur est survenue avec la BDD" + err);
})

module.exports = connexion