const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    register_date: { type: String, required: true, default: new Date().toLocaleDateString() + "" + new Date().toLocaleDateString },
    // token_valid: {type: String},
    // role: {type: String, required: true, default: "User"}
});

exports.User = mongoose.model('User', UserSchema);