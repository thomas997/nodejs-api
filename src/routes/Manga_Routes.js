const router = require('express').Router()
const MangaController = require('../controllers/Manga_Controller');

router.post('/createManga', MangaController.createMangas);
router.get('/getAllMangas', MangaController.getAllMangas);
router.get('/getManga/:id', MangaController.getManga);
router.put('/updateManga/:id', MangaController.updateMangas);
router.delete('/deleteMangas/:id', MangaController.deleteMangas);

module.exports = router;