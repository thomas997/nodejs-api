const router = require('express').Router();
const userController = require('../controllers/User_Controller');
const validateToken = require('../providers/validateToken');

router.post('/signUp', userController.signUp);
router.post('/signIn', userController.signIn);
router.get('/get/:id', userController.getUsers);
router.get('/getAll', userController.getAllUsers);
router.put('/update/:id', userController.update);
router.delete('/delete/:id', userController.deleteUser);
// router.post('/validateToken', validateToken.validateToken);

module.exports = router;