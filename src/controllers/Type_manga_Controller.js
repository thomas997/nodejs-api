const TypeMangas = require('../models/types_mangas').TypeMangas;
const Manga = require('../models/Mangas').Mangas;

function responseOkToSend(newManga, msg, res) {
    res.status(200).json({
        message: msg,
        manga: newManga
    })
}

function responseErrorToSend(err, msg, res) {
    res.status(500).json({
        message: msg,
        err: err
    })
}

exports.createTypeMangas = (req, res) => {
    const newTypeManga = new TypeMangas({
        name: req.body.name
    })
    newTypeManga.save().then(promesse_type_manga => {
        responseOkToSend(promesse_type_manga, "type de Manga bien créé.", res);
    }).catch(err => {
        responseErrorToSend(err, "erreur lors de la création du type de manga.", res);
    })
}

exports.updateTypeMangas = (req, res) => {
    TypeMangas.findByIdAndUpdate(req.params.id, {
        name: req.body.name
    }).then(promesse_type_manga => {
        res.status(200).json({
            message: "Le type de manga a bien été update.",
            manga: promesse_type_manga
        })
    }).catch(err => {
        res.status(500).json({
            message: "Une erreur s'est produite à l'update du type de manga.",
            err: err
        })
    })
}

exports.getAllTypeMangas = (req, res) => {
    TypeMangas.find().then(promesse_type_mangas => {
        res.status(200).json({
            message: "tous les types de mangas ont bien été séléctionnés.",
            mangas: promesse_type_mangas
        })
    }).catch(err => {
        res.status(500).json({
            message: "Une erreur s'est produite lors de la séléction de tous les types de mangas.",
            err: err
        })
    })
}

exports.deleteTypeMangas = (req, res) => {
    const deleteById = req.params.id
    TypeMangas.findByIdAndDelete(deleteById)
        .then(TypeMangas => {
            res.status(201).json({
                messsage: "le type de manga a bien été supprimé.",
                Mangas: TypeMangas
            })
        }).catch(err => res.status(500).json({
            err: err
        }))
}