const Mangas = require('../models/Mangas').Mangas;
// const validateToken = require('../providers/validateToken').validateToken;
// const validateRole = require('../providers/validateRole').validateRole;

function responseOkToSend(newManga, msg, res) {
    res.status(200).json({
        message: msg,
        manga: newManga
    })
}

function responseErrorToSend(err, msg, res) {
    res.status(500).json({
        message: msg,
        err: err
    })
}

// exports.createMangas = (req, res) => {
//     const newManga = new Mangas({
//         name: req.body.name
//     })
//     const token = req.headers.authorization
//     if(validateToken(req, token) == "Token bien valide"){
//         const token_split = token.split(" ")[1];
//         console.log(validateRole(token_split));
//         if(validateRole(token_split) == "Admin"){
//             newManga.save().then(promesse_manga => {
//                 responseOkToSend(promesse_manga, "Manga bien créé.", res);
//             }).catch(err => {
//                 responseErrorToSend(err, "erreur lors de la création du manga.", res);
//             })
//         }else {
//             res.status(500).json({
//                 message: "vous n'avez pas les droits pour effectuer cette action."
//             })
//         }
//     } else{
//         res.status(500).json({
//             message: validateToken(req, token)
//         })
//     }
// }

exports.createMangas = (req, res) => {
    const newManga = new Mangas({
        name: req.body.name,
        id_type_manga: req.body.id_type_manga,
        author: req.body.author,
        url: req.body.url
    })
    newManga.save().then(promesse_manga => {
        responseOkToSend(promesse_manga, "Manga bien créé.", res);
    }).catch(err => {
        responseErrorToSend(err, "erreur lors de la création du manga.", res);
    })
}


// méthode pour update le manga 
exports.updateMangas = (req, res) => {
    Mangas.findByIdAndUpdate(req.params.id, {
        name: req.body.name,
        author: req.body.author,
        id_type_manga: req.body.id_type_manga
    }).then(promesse_manga => {
        res.status(200).json({
            message: "Le manga a bien été update.",
            manga: promesse_manga
        })
    }).catch(err => {
        res.status(500).json({
            message: "Une erreur s'est produite à l'update du manga.",
            err: err
        })
    })
}

exports.getAllMangas = (req, res) => {
    Mangas.find().then(promesse_mangas => {
        res.status(200).json({
            message: "tous les mangas ont bien été séléctionnés.",
            mangas: promesse_mangas
        })
    }).catch(err => {
        res.status(500).json({
            message: "Une erreur s'est produite lors de la séléction de tous les mangas.",
            err: err
        })
    })
}

exports.getManga = (req, res) => {
    Mangas.findById(req.params.id).then(promesse_manga => {
        res.status(200).json({
            message: "manga bien selectionné :)",
            mangas: promesse_manga
        })
    }).catch(err => {
        res.status(500).json({
            message: "Une erreur s'est produite lors de la séléction du manga",
            err: err
        })
    })
}

exports.deleteMangas = (req, res) => {
    const deleteById = req.params.id
    Mangas.findByIdAndDelete(deleteById)
        .then(Mangas => {
            res.status(201).json({
                messsage: "le manga est bien supprimé.",
                Mangas: Mangas
            })
        }).catch(err => res.status(500).json({
            err: err
        }))
}

// methode qui fait une requete en bdd pour rechercher une cat  egorie en fonction de l'id
exports.getMangasByTypeMangaId = (req, res) => {
    Mangas.find({ id_type_manga: req.params.id_type_manga })
        .then(mangas => res.status(200).json({ mangas: mangas }))
        .catch(err => { res.status(404).json({ err: err }) })
}