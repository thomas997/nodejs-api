const jwt = require('jsonwebtoken');

exports.validateRole = (token) => {
    const tokenPayLoad = jwt.decode(token);
    return tokenPayLoad.role
}