const jwt = require('jsonwebtoken');
require('dotenv').config();


exports.validateToken = (req, token) => {
    let result;
    if(token) {
        const token_split = token.split(" ")[1];
        try {
            const tokenVerified = jwt.verify(token_split, process.env.JWT_SECRET_TOKEN);
            req.user = tokenVerified;
            result = "Token bien valide";
        }catch(err) {
            result = "Token invalide" + err
        }
    }else {
        result = "utilisateur non valide"
    }
    return result
}